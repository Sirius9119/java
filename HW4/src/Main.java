public class Main {
    public static void main(String[] args) {

        Pet dog = new Pet("dog", "Sharik", 5, 85, new String[]{"eat", "drink", "sleep"});
        Human mother = new Human("Jane", "Doe", 1980);
        Human father = new Human("John", "Doe", 1978);
        Human child = new Human("Alice", "Doe", 2005);
        String[][] schedule = {{"Monday", "work"}, {"Tuesday", "gym"}};
        Human familyChild = new Human("Bob", "Doe", 2010, 120, mother, father, dog, schedule);

        Family family = new Family(mother, father);
        family.addChild(child);
        family.setPet(dog);

        System.out.println(dog);
        System.out.println(mother);
        System.out.println(father);
        System.out.println(child);
        System.out.println(familyChild);
        System.out.println(family);
    }
}