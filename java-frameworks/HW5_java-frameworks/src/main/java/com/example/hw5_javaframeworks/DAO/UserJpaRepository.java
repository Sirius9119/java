package com.example.hw5_javaframeworks.DAO;


import com.example.hw5_javaframeworks.security.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserJpaRepository extends JpaRepository<SysUser, Long> {
    Optional<SysUser> findUsersByUserName(String userName);

}
