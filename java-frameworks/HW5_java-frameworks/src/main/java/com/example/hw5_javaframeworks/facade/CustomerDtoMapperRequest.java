package com.example.hw5_javaframeworks.facade;


import com.example.hw5_javaframeworks.dto.customer.CustomerDtoRequest;
import com.example.hw5_javaframeworks.models.Customer;
import com.example.hw5_javaframeworks.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class CustomerDtoMapperRequest extends DtoMapperFacade<Customer, CustomerDtoRequest> {

    public CustomerDtoMapperRequest() {
        super(Customer.class, CustomerDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Customer entity, CustomerDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAge(dto.getAge());
        entity.setEmail(dto.getEmail());
        entity.setPhone(dto.getPhone());
    }
}
