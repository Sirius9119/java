package com.example.hw5_javaframeworks.facade;


import com.example.hw5_javaframeworks.dto.employer.EmployerDtoRequest;
import com.example.hw5_javaframeworks.models.Employer;
import com.example.hw5_javaframeworks.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class EmployerDtoMapperRequest extends DtoMapperFacade<Employer, EmployerDtoRequest> {

    public EmployerDtoMapperRequest() {
        super(Employer.class, EmployerDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Employer entity, EmployerDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAddress(dto.getAddress());
    }
}
