package com.example.hw5_javaframeworks.domain;

import lombok.Data;

@Data
public class HelloMessage {
    private String name;
}
