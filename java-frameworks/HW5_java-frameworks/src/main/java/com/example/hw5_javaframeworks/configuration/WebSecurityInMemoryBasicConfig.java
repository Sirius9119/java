package com.example.hw5_javaframeworks.configuration;

//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.crypto.password.PasswordEncoder;
//
//@Configuration
//@EnableWebSecurity
//public class WebSecurityInMemoryBasicConfig extends WebSecurityConfigurerAdapter {
////
////    @Override
////    protected void configure(HttpSecurity http) throws Exception {
////        http.csrf().disable()
////                .authorizeRequests()
//////                .antMatchers("/").permitAll()
//////                .antMatchers("/dashboard").hasAnyRole("USER")
////                .anyRequest().authenticated()
////                .and()
////                .httpBasic()
////                .and()
////                .logout()// default url /logout (не поддерживается)
////                .invalidateHttpSession(true)
////                .logoutSuccessUrl("http://BADNAME@localhost:9000/login") // хак
////                .permitAll();
////    }
//
//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth, PasswordEncoder passwordEncoder) throws Exception {
//        auth
//                .inMemoryAuthentication()
//                    .withUser("a")
//                    .password(passwordEncoder.encode("a"))
//                    .roles("USER")
//                .and()
//                    .withUser("b")
//                    .password(passwordEncoder.encode("b"))
//                    .roles("USER");
//    }
//
//
//}
