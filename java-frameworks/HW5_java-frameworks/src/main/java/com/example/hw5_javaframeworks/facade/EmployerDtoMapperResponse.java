package com.example.hw5_javaframeworks.facade;


import com.example.hw5_javaframeworks.dto.employer.EmployerDtoResponse;
import com.example.hw5_javaframeworks.models.Employer;
import com.example.hw5_javaframeworks.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class EmployerDtoMapperResponse extends DtoMapperFacade<Employer, EmployerDtoResponse> {
    public EmployerDtoMapperResponse() {
        super(Employer.class, EmployerDtoResponse.class);
    }

    @Override
    protected void decorateDto(EmployerDtoResponse dto, Employer entity) {
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAddress(entity.getAddress());
        dto.setCustomers(entity.getCustomers());
    }
}
