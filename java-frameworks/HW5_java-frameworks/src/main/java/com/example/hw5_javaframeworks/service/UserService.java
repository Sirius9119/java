package com.example.hw5_javaframeworks.service;


import com.example.hw5_javaframeworks.DAO.UserJpaRepository;
import com.example.hw5_javaframeworks.security.SysUser;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserJpaRepository userJpaRepository;

    public Optional<SysUser> getByLogin(@NonNull String login) {

        return userJpaRepository.findUsersByUserName(login);
    }

}