package com.example.hw4_javaframeworks.repo;


import com.example.hw4_javaframeworks.configuration.security.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findUsersByUserName(String userName);

}
