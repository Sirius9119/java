package com.example.hw4_javaframeworks.facade;


import com.example.hw4_javaframeworks.Service.DtoMapperFacade;
import com.example.hw4_javaframeworks.dto.employer.EmployerDtoResponse;
import com.example.hw4_javaframeworks.models.Employer;
import org.springframework.stereotype.Service;

@Service
public class EmployerDtoMapperResponse extends DtoMapperFacade<Employer, EmployerDtoResponse> {
    public EmployerDtoMapperResponse() {
        super(Employer.class, EmployerDtoResponse.class);
    }

    @Override
    protected void decorateDto(EmployerDtoResponse dto, Employer entity) {
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAddress(entity.getAddress());
//        dto.setCustomers(entity.getCustomers());
    }


}
