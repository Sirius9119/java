package com.example.hw4_javaframeworks.facade;


import com.example.hw4_javaframeworks.Service.DtoMapperFacade;
import com.example.hw4_javaframeworks.dto.employer.EmployerDtoRequest;
import com.example.hw4_javaframeworks.models.Employer;
import org.springframework.stereotype.Service;

@Service
public class EmployerDtoMapperRequest extends DtoMapperFacade<Employer, EmployerDtoRequest> {

    public EmployerDtoMapperRequest() {
        super(Employer.class, EmployerDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Employer entity, EmployerDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAddress(dto.getAddress());
    }


}
