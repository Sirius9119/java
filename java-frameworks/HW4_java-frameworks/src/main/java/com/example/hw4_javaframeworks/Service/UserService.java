package com.example.hw4_javaframeworks.Service;


import com.example.hw4_javaframeworks.configuration.security.User;
import com.example.hw4_javaframeworks.repo.UserRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public Optional<User> getByLogin(@NonNull String login) {

        return userRepository.findUsersByUserName(login);
    }

}