package com.example.hw4_javaframeworks.facade;


import com.example.hw4_javaframeworks.Service.DtoMapperFacade;
import com.example.hw4_javaframeworks.dto.customer.CustomerDtoRequest;
import com.example.hw4_javaframeworks.models.Customer;
import org.springframework.stereotype.Service;

@Service
public class CustomerDtoMapperRequest extends DtoMapperFacade<Customer, CustomerDtoRequest> {

    public CustomerDtoMapperRequest() {
        super(Customer.class, CustomerDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Customer entity, CustomerDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAge(dto.getAge());
        entity.setEmail(dto.getEmail());
        entity.setPhone(dto.getPhone());
        entity.setPassword(dto.getPassword());
        entity.setAccounts(dto.getAccounts());
        entity.setEmployers(dto.getEmployers());
    }


}
