package com.example.hw4_javaframeworks.facade;


import com.example.hw4_javaframeworks.Service.DtoMapperFacade;
import com.example.hw4_javaframeworks.dto.account.AccountDtoRequest;
import com.example.hw4_javaframeworks.models.Account;
import org.springframework.stereotype.Service;

@Service
public class AccountDtoMapperRequest extends DtoMapperFacade<Account, AccountDtoRequest> {

    public AccountDtoMapperRequest() {
        super(Account.class, AccountDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Account entity, AccountDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setNumber(dto.getNumber());
        entity.setCurrency(dto.getCurrency());
        entity.setBalance(dto.getBalance());
    }
}
