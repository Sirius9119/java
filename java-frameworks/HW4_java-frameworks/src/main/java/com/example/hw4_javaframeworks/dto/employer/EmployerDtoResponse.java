package com.example.hw4_javaframeworks.dto.employer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployerDtoResponse {

    private Long id;
    private String name;
    private String address;
}
