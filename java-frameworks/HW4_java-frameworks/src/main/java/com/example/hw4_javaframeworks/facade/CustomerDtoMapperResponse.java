package com.example.hw4_javaframeworks.facade;


import com.example.hw4_javaframeworks.Service.DtoMapperFacade;
import com.example.hw4_javaframeworks.dto.customer.CustomerDtoResponse;
import com.example.hw4_javaframeworks.models.Customer;
import org.springframework.stereotype.Service;

@Service
public class CustomerDtoMapperResponse extends DtoMapperFacade<Customer, CustomerDtoResponse> {
    public CustomerDtoMapperResponse() {
        super(Customer.class, CustomerDtoResponse.class);
    }

    @Override
    protected void decorateDto(CustomerDtoResponse dto, Customer entity) {
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAge(entity.getAge());
        dto.setEmail(entity.getEmail());
        dto.setAccounts(entity.getAccounts());
        dto.setEmployers(entity.getEmployers());
    }


}
