package com.example.hw4_javaframeworks.DAO;


import com.example.hw4_javaframeworks.models.Employer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployerJpaRepository extends JpaRepository<Employer, Long> {

}
