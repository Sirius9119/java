package com.example.hw3_javaframeworks.repo;

import com.example.hw3_javaframeworks.models.Employer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployerRepo extends JpaRepository<Employer, Long> {}
