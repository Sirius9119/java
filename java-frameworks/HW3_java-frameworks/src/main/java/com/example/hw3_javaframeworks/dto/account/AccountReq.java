package com.example.hw3_javaframeworks.dto.account;

import com.example.hw3_javaframeworks.models.Currency;
import com.example.hw3_javaframeworks.models.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountReq {
    private Currency currency;
    private Customer customer;
}
