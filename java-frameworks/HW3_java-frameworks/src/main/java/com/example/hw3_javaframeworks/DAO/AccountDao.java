package com.example.hw3_javaframeworks.DAO;

import com.example.hw3_javaframeworks.models.Account;

public interface AccountDao extends Dao<Account> {
}
