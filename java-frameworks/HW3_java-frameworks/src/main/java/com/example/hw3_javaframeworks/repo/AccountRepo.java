package com.example.hw3_javaframeworks.repo;

import com.example.hw3_javaframeworks.models.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepo extends JpaRepository<Account, Long> {
    List<Account> findByCustomer_Id(Long cId);
}
