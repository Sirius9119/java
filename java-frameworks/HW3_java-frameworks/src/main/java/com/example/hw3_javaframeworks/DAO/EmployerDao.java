package com.example.hw3_javaframeworks.DAO;

import com.example.hw3_javaframeworks.models.Employer;

public interface EmployerDao extends Dao<Employer> {
}

