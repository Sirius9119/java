package com.example.hw3_javaframeworks.facade;
import com.example.hw3_javaframeworks.dto.customer.CustomerReq;
import com.example.hw3_javaframeworks.dto.customer.CustomerResp;
import com.example.hw3_javaframeworks.models.Customer;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomerFacade {
    private final ModelMapper modelMapper;

    public CustomerFacade(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public Customer toEntity(CustomerReq customerReq) {
        return modelMapper.map(customerReq, Customer.class);
    }

    public CustomerResp toResponse(Customer customer) {
        return modelMapper.map(customer, CustomerResp.class);
    }
}
