package com.example.hw3_javaframeworks.models;

public enum Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP
}
