package com.example.hw3_javaframeworks.Service;

import com.example.hw3_javaframeworks.dto.employer.EmployerResp;
import com.example.hw3_javaframeworks.models.Customer;
import com.example.hw3_javaframeworks.models.Employer;
import com.example.hw3_javaframeworks.repo.CustomerRepo;
import com.example.hw3_javaframeworks.repo.EmployerRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployerService {
    private final EmployerRepo employerRepo;
    private final CustomerRepo customerRepo;

    public List<EmployerResp> getAll() {
        return employerRepo.findAll()
                .stream()
                .map(e -> {
                    EmployerResp ee = new EmployerResp() {{
                        setId(e.getId());
                        setName(e.getName());
                        setAddress(e.getAddress());
                        setCustomers(e.getCustomers());
                    }};
                    return ee;
                }).toList();
    }

    public Optional<Employer> findById(Long id) {
        return employerRepo.findById(id);
    }

    public Employer save(Long cId, Employer employer) {
        Employer e = customerRepo.findById(cId).map(c -> {
            c.addEmployer(employer);
            return employerRepo.save(employer);
        }).orElseThrow(() -> new RuntimeException("Employer wasn't saved"));

        return employerRepo.save(e);
    }

    public boolean  deleteEmployerFromCustomer(Long cId, Long eId) {
        try {
            Customer customer = customerRepo.findById(cId).orElseThrow(() -> new RuntimeException("Customer not found"));
            customer.removeEmployer(eId);
            customerRepo.save(customer);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean deleteById(Long id) {
        try {
            employerRepo.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}