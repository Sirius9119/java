package com.example.hw3_javaframeworks.Service;

import com.example.hw3_javaframeworks.dto.customer.CustomerReq;
import com.example.hw3_javaframeworks.dto.customer.CustomerResp;
import com.example.hw3_javaframeworks.facade.CustomerFacade;
import com.example.hw3_javaframeworks.models.Customer;
import com.example.hw3_javaframeworks.repo.CustomerRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepo customerRepo;
    private final CustomerFacade customerFacade;


    public List<CustomerResp> getAll() {
        return customerRepo.findAll()
                .stream()
                .map(customerFacade::toResponse)
                .toList();
    }
    public Page<Customer> findAll(Pageable pageable) {
        return customerRepo.findAll(pageable);
    }

    public Customer save(CustomerReq c) {
        Customer customer = customerFacade.toEntity(c);
        return customerRepo.save(customer);
    }

    public Optional<Customer> findById(Long id) {
        return customerRepo.findById(id);
    }
    public void update(Long id, CustomerReq c) throws RuntimeException {
        Optional<Customer> cc = customerRepo.findById(id);
        cc.ifPresentOrElse(
                x -> {
                    Customer updatedCustomer = customerFacade.toEntity(c);
                    updatedCustomer.setId(id);
                    customerRepo.save(updatedCustomer);
                },
                () -> {
                    throw new RuntimeException("Customer doesn't exist!");
                }
        );
    }

    public boolean deleteById(Long id) {
        try {
            customerRepo.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void deleteAll() {
        customerRepo.deleteAll();
    }
}