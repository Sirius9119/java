package com.example.hw3_javaframeworks.DAO;

import com.example.hw3_javaframeworks.models.Customer;

public interface CustomerDao extends Dao<Customer> {
}
