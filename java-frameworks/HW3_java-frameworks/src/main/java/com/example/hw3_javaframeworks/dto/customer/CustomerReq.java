package com.example.hw3_javaframeworks.dto.customer;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomerReq {
    @NotBlank
    @Size(min = 2)
    private String name;
    @NotBlank
    @Email
    private String email;
    @Min(18)
    private Integer age;
    @NotBlank
    @Pattern(regexp = "\\+?\\d+")
    private String phoneNumber;
}
