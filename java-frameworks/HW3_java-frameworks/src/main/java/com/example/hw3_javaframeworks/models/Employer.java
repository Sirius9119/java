package com.example.hw3_javaframeworks.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@Entity
@Table(name="employers")
@EqualsAndHashCode(callSuper=false)
public class Employer extends AbstractEntity{
    private String name;
    private String address;

    @ManyToMany(mappedBy = "employers")
    @JsonIgnoreProperties("employers")
    private List<Customer> customers;
}
