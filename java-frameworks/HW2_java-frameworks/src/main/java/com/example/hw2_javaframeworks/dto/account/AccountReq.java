package com.example.hw2_javaframeworks.dto.account;

import com.example.hw2_javaframeworks.models.Currency;
import com.example.hw2_javaframeworks.models.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountReq {
    private Currency currency;
    private Customer customer;
}
