package com.example.hw2_javaframeworks.dto.customer;

import com.example.hw2_javaframeworks.models.Account;
import com.example.hw2_javaframeworks.models.Employer;
import lombok.Data;

import java.util.List;

@Data
public class CustomerResp {
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private List<Account> accounts;
    private List<Employer> employers;
}
