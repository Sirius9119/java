package com.example.hw2_javaframeworks.Service;

import com.example.hw2_javaframeworks.dto.customer.CustomerReq;
import com.example.hw2_javaframeworks.dto.customer.CustomerResp;
import com.example.hw2_javaframeworks.models.Customer;
import com.example.hw2_javaframeworks.repo.CustomerRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepo customerRepo;

    public List<CustomerResp> getAll() {
        return customerRepo.findAll()
                .stream()
                .map(c -> {
                    CustomerResp cc = new CustomerResp() {{
                        setId(c.getId());
                        setName(c.getName());
                        setEmail(c.getEmail());
                        setAge(c.getAge());
                        setAccounts(c.getAccounts());
                        setEmployers(c.getEmployers());
                    }};
                    return cc;
                }).toList();
    }

    public Customer save(CustomerReq c) {
        Customer cc = new Customer();
        cc.setName(c.getName());
        cc.setEmail(c.getEmail());
        cc.setAge(c.getAge());
        customerRepo.save(cc);
        return cc;
    }

    public Optional<Customer> findById(Long id) {
        return customerRepo.findById(id);
    }

    public void update(Long id, Customer c) throws RuntimeException {
        Optional<Customer> cc = customerRepo.findById(id);
        cc.ifPresentOrElse(
                x -> {
                    x.setName(c.getName());
                    x.setEmail(c.getEmail());
                    x.setAge(c.getAge());

                    customerRepo.save(x);
                },
                () -> {
                    throw new RuntimeException("Customer does`t exist!");
                }
        );
    }

    public boolean deleteById(Long id) {
        try {
            customerRepo.deleteById(id);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public void deleteAll() {
        customerRepo.deleteAll();
    }
}