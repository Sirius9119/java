package com.example.hw2_javaframeworks.models;

public enum Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP
}
