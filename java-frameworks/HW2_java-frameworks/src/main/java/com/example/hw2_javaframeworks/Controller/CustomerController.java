package com.example.hw2_javaframeworks.Controller;

import com.example.hw2_javaframeworks.Service.CustomerService;
import com.example.hw2_javaframeworks.dto.customer.CustomerReq;
import com.example.hw2_javaframeworks.dto.customer.CustomerResp;
import com.example.hw2_javaframeworks.models.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class CustomerController {
    private final CustomerService customerService;

    @GetMapping
    public List<CustomerResp> getAllCustomers() {
        return customerService.getAll();
    }

    @GetMapping("/{id}")
    public Optional<Customer> getCustomer(@PathVariable("id") Long id){
        return customerService.findById(id);
    }

    @PostMapping
    public ResponseEntity<Customer> createCustomer(@RequestBody CustomerReq c) {
        return ResponseEntity.ok().body(customerService.save(c));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable("id") Long id, @RequestBody Customer c) {
        try {
            customerService.update(id, c);
            return ResponseEntity.ok().build();

        } catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("id") Long id) {
        return customerService.deleteById(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }
}
