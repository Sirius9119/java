package com.example.hw2_javaframeworks.DAO;

import com.example.hw2_javaframeworks.models.Customer;

public interface CustomerDao extends Dao<Customer> {
}
