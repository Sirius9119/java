package com.example.hw2_javaframeworks.dto.employer;

import com.example.hw2_javaframeworks.models.Customer;
import lombok.Data;

import java.util.List;

@Data
public class EmployerResp {
    private Long id;
    private String name;
    private String address;
    private List<Customer> customers;
}
