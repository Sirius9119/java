package com.example.hw2_javaframeworks.DAO;

import com.example.hw2_javaframeworks.models.Employer;

public interface EmployerDao extends Dao<Employer> {
}

