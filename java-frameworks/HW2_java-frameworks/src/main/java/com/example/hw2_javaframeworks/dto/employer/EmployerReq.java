package com.example.hw2_javaframeworks.dto.employer;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployerReq {
    private String name;
    private String address;
}
