package com.example.hw2_javaframeworks.repo;

import com.example.hw2_javaframeworks.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Long> {}
