package com.example.hw2_javaframeworks.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import jakarta.persistence.*;
import java.util.UUID;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name="accounts")
@EqualsAndHashCode(callSuper=false)
public class Account extends AbstractEntity{
    private String number = UUID.randomUUID().toString();
    @Enumerated(EnumType.STRING)
    private Currency currency;
    private Double balance = (double) 0;

    @ManyToOne
    @JsonIgnoreProperties("accounts")
    private Customer customer;
}