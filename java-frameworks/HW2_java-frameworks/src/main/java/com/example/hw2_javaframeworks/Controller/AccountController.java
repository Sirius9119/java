package com.example.hw2_javaframeworks.Controller;

import com.example.hw2_javaframeworks.dto.account.AccountResp;
import com.example.hw2_javaframeworks.models.Account;
import com.example.hw2_javaframeworks.Service.AccountService;
import com.example.hw2_javaframeworks.Service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class AccountController {
    private final AccountService accountService;
    private final CustomerService customerService;

    @GetMapping
    public List<AccountResp> getAllAccounts() {
        return accountService.getAll();
    }

    @GetMapping("/{id}")
    public Optional<Account> getAccount(@PathVariable("id") Long id) {
        return accountService.findById(id);
    }

    @PutMapping("/deposit/{number}")
    public ResponseEntity<Account> addFunds(@PathVariable("number") String number, @RequestBody Double amount) {
        try {
            Account account = accountService.addFunds(number, amount);
            return ResponseEntity.ok(account);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("/withdraw/{number}")
    public ResponseEntity<Account> withdraw(@PathVariable("number") String number, @RequestBody Double amount) {
        try {
            Account account = accountService.withdraw(number, amount);
            return ResponseEntity.ok(account);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/transfer/{from}/{to}")
    public ResponseEntity<Boolean> transfer(@PathVariable("from") String from, @PathVariable("to") String to, @RequestBody Double amount) {
        Account accountFrom = accountService.getByNumber(from);
        Account accountTo = accountService.getByNumber(to);
        boolean b = accountService.transfer(from, to, amount);
        if (b){
            return ResponseEntity.ok(b);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/customers/{customerId}")
    public ResponseEntity<Account> createAccount(@PathVariable("customerId") Long cId, @RequestBody Account a) {
        Account result = accountService.save(cId, a);
        return ResponseEntity.ok().body(result);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAccount(@PathVariable("id") Long id) {
        return accountService.deleteById(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }
}