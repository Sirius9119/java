package com.example.hw2_javaframeworks.Service;


import com.example.hw2_javaframeworks.dto.account.AccountResp;
import com.example.hw2_javaframeworks.models.Account;
import com.example.hw2_javaframeworks.repo.AccountRepo;
import com.example.hw2_javaframeworks.repo.CustomerRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepo accountRepo;
    private final CustomerRepo customerRepo;

    public List<AccountResp> getAll() {
        return accountRepo.findAll()
                .stream()
                .map(a -> {
                    AccountResp aa = new AccountResp() {{
                        setId(a.getId());
                        setNumber(a.getNumber());
                        setCurrency(a.getCurrency());
                        setBalance(a.getBalance());
                        setCustomer(a.getCustomer());
                    }};
                    return aa;
                }).toList();
    }

    public Account save(Long cId, Account account) {
        Account a = customerRepo.findById(cId).map(c -> {
            account.setCustomer(c);
            return accountRepo.save(account);
        }).orElseThrow(() -> new RuntimeException("Account wasn't added"));
        return accountRepo.save(a);
    }

    public Optional<Account> findById(Long id) {
        return accountRepo.findById(id);
    }

    public Account getByNumber(String number) throws RuntimeException {
        try {
            return accountRepo.findAll().stream().filter(a -> Objects.equals(a.getNumber(), number))
                    .findFirst().orElseThrow();
        } catch (Exception e) {
            throw new RuntimeException("Account doesn't exist!");
        }
    }

    public void update(Long id, Account a) throws RuntimeException {
        Optional<Account> aa = accountRepo.findById(id);
        aa.ifPresentOrElse(
                x -> {
                    x.setId(a.getId());
                    x.setNumber(a.getNumber());
                    x.setCurrency(a.getCurrency());
                    x.setBalance(a.getBalance());
                    x.setCustomer(a.getCustomer());

                    accountRepo.save(a);
                },
                () -> {
                    throw new RuntimeException("Account does`t exist!");
                }
        );
    }

    public boolean delete(Account obj) {
        return false;
    }

    public boolean deleteById(Long id) {
        try {
            accountRepo.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void deleteAll(List<Account> accounts) {
        accountRepo.deleteAll();
    }

    public Account addFunds(String n, Double amount) {
        Account a = getByNumber(n);
        Double b = a.getBalance();
        a.setBalance(b + amount);

        update(a.getId(), a);
        return a;
    }

    public Account withdraw(String n, Double amount) throws RuntimeException {
        Account a = getByNumber(n);
        Double b = a.getBalance();
        if(b < amount) throw new RuntimeException("Not enough money on your account!");
        a.setBalance(b - amount);

        update(a.getId(), a);
        return a;
    }

    public boolean transfer(String from, String to, Double amount) {
        Account accountFrom = getByNumber(from);
        Account accountTo = getByNumber(to);
        Double balanceFrom = accountFrom.getBalance();
        if (balanceFrom < amount) return false;
        withdraw(from, amount);
        addFunds(to, amount);
        return true;
    }
}