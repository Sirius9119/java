package com.example.hw2_javaframeworks.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.ManyToMany;

import java.util.List;

@Data
@Entity
@Table(name="employers")
@EqualsAndHashCode(callSuper=false)
public class Employer extends AbstractEntity{
    private String name;
    private String address;

    @ManyToMany(mappedBy = "employers")
    @JsonIgnoreProperties("employers")
    private List<Customer> customers;
}
