package com.example.hw2_javaframeworks.DAO;

import com.example.hw2_javaframeworks.models.Account;

public interface AccountDao extends Dao<Account> {
}
