import React, { useEffect, useState } from 'react';
import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from './Title';
import AccountForm from './AccountForm';
import CustomerForm from './CustomerForm';

export default function Customers() {
  const [customers, setCustomers] = useState([]);
  const [editingCustomerId, setEditingCustomerId] = useState(null);
  const [addACustomerId, setAddACustomerId] = useState(null);

  useEffect(() => {
    fetchCustomers();
  }, []);
  
  const fetchCustomers = async () => {
    try {
      const response = await fetch('http://localhost:9000/customers');
      const data = await response.json();
      setCustomers(data);
    } catch (error) {
      console.error('Error fetching customers:', error);
    }
  };


  const updateCustomer = async (customerId, updatedCustomer) => {
    try {
      const response = await fetch(`http://localhost:9000/customers/${customerId}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedCustomer),
      });

      if (response.ok) {
        const customer = await response.json();

        setCustomers((prev) => prev.map((c) => (
            c.id !== customerId ? c : customer)))

                alert('Updating successful');
                  }else {
                 alert('Failed. Please try again.');
            }
        setEditingCustomerId(null);
      }
     catch (error) {
      console.error('Error updating customer:', error);
    }
  };

  const deleteCustomer = async (customerId) => {
    try {
      const response = await fetch(`http://localhost:9000/customers/${customerId}`, {
        method: 'DELETE',
      });

      if (response.ok) {
                alert('Delete successful');
                  }else {
                 alert('Failed. Please try again.');
            }
    } catch (error) {
      console.error('Error deleting customer:', error);
    }
  };

  const addAccountToCustomer = async (customerId, currency) => {

    try {
      const response = await fetch(`http://localhost:9000/customers/${customerId}/accounts`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(currency),
      });

      if (response.ok) {
        alert('Account added');
          }else {
         alert('Failed. Please try again.');
    }
    } catch (error) {
      console.error('Error adding account to customer:', error);
    }
  }

  const depositToAccount = async (customerId, aNumber, amount) => {
    try {
      const response = await fetch(`http://localhost:9000/accounts/deposit/${aNumber}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(amount),
      });

      if (response.ok) {
        const { balance } = await response.json();

        setCustomers((prev) => prev.map((c) => (
          c.id !== customerId ? c : {
            ...c,
            accounts: c.accounts.map((a) => (
              a.number !== aNumber ? a : {
                ...a,
                balance
              }))
          })))

        alert('Deposit successful');
      } else {
        alert('Deposit failed. Please try again.');
      }

    } catch (error) {
      console.error('Error depositing to account:', error);
    }
  };

  const withdrawFromAccount = async (customerId, aNumber, amount) => {
    try {
      const response = await fetch(`http://localhost:9000/accounts/withdraw/${aNumber}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(amount),
      });

     if (response.ok) {
       alert('Withdraw successful');
         }else {
        alert('Not enough money. Please try again.');
    }
    } catch (error) {
      console.error('Error withdrawing from account:', error);
    }
  };

  const handleEditButtonClick = (customerId) => {
    setEditingCustomerId(customerId);
  };

  const handleCancelEdit = () => {
    setEditingCustomerId(null);
  };

  const handleSaveCustomer = (customer) => {
    if (editingCustomerId) {
      updateCustomer(editingCustomerId, customer);
    }
  };

const handleSaveNewAccount = (customerId, currency) => {
    if(currency !== ""){
    addAccountToCustomer(addACustomerId, currency)
    }
  };

const handleDeposit = async (customerId, accountId) => {
    const amount = prompt('Enter amount to deposit');
    if (amount<=0) {
            alert("Amount can't be less than 0");
            return;
          }
    if (amount !== null && amount !== "") {
        depositToAccount(customerId, accountId, amount);
    }
};

  const handleWithdraw = (customerId, accountId, amount) => {
  if (amount<=0) {
              alert("Amount can't be less than 0");
              return;
            }
    withdrawFromAccount(customerId, accountId, amount);
  };

  return (
    <React.Fragment>
      {customers.map((customer) => (
        <React.Fragment key={customer.id}>
          {editingCustomerId === customer.id ? (
            <React.Fragment>
              <Title>Editing {customer.name}</Title>
              <CustomerForm
                onSave={handleSaveCustomer}
                onCancel={handleCancelEdit}
                customer={customer}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Title>
                {customer.name} (email: {customer.email}, age: {customer.age})
              </Title>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell>Account Number</TableCell>
                    <TableCell>Currency</TableCell>
                    <TableCell>Balance</TableCell>
                    <TableCell>Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {customer.accounts.map((account) => (
                    <TableRow key={account.id}>
                      <TableCell>{account.number}</TableCell>
                      <TableCell>{account.currency}</TableCell>
                      <TableCell>{account.balance}</TableCell>
                      <TableCell>
                        <button onClick={() => handleDeposit(customer.id, account.number)}>
                          Deposit
                        </button>
                        <button onClick={() => handleWithdraw(customer.id, account.number, prompt('Enter amount to withdraw'))}>
                          Withdraw
                        </button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>

              <React.Fragment>
                {addACustomerId === customer.id ? (
                  <AccountForm
                    onSave={(customerId, currency) => handleSaveNewAccount(customer.id, currency)}
                    onCancel={() => setAddACustomerId(null)}
                    customer={customer}
                    id={customer.id}
                  />
                ) : <Link
                    color="primary"
                    href="#"
                    onClick={() => setAddACustomerId(customer.id)}
                    sx={{ mt: 3 }}
                  >
                    Add Account
              </Link>}
                </React.Fragment>
              <Link
                color="primary"
                href="#"
                onClick={() => handleEditButtonClick(customer.id)}
                sx={{ mt: 3 }}
              >
                Edit Customer
              </Link>
              <button onClick={() => deleteCustomer(customer.id)}>Delete Customer</button>
            </React.Fragment>

          )}
        </React.Fragment>
      ))}
    </React.Fragment>
  );
};