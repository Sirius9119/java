package com.example.hw1_javaframeworks.Controller;

import com.example.hw1_javaframeworks.Service.CustomerService;
import lombok.RequiredArgsConstructor;
import com.example.hw1_javaframeworks.models.Currency;
import com.example.hw1_javaframeworks.models.Customer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
@CrossOrigin(origins = {"http://localhost:3000"})

public class CustomerController {

    private final CustomerService customerService;

    @GetMapping
    public List<Customer> getAllCustomers() {
        return customerService.getAllCustomers();

    }

    @GetMapping("/{id}")
    public Customer getCustomerById(@PathVariable long id) {
        System.out.println(id);
        return customerService.getCustomerById(id);
    }


    @PostMapping
    public void createCustomer(@RequestBody Customer customer) {
        customerService.createCustomer(customer);
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable long id, @RequestBody Map<String, Object> customerData) {
        try {
            String name = (String) customerData.get("name");
            String email = (String) customerData.get("email");
            int age = (int) customerData.get("age");
            Customer updatedCustomer = customerService.updateCustomer(id, name, email, age);
            return ResponseEntity.ok(updatedCustomer);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to update customer.");
        }
    }


    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable long id) {
        customerService.deleteCustomer(id);
    }

    @PostMapping("/{id}/accounts")
    public void createAccount(@PathVariable("id") Long id, @RequestBody Currency currency) {
         customerService.createAccount(id, currency);
    }

    @DeleteMapping("/{id}/accounts/{accountId}")
    public void deleteAccount(@PathVariable("id") Long id, @PathVariable("accountId") Long accId) {
        customerService.deleteAccount(id, accId);
    }
}