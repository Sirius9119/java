package com.example.hw1_javaframeworks.Service;

import com.example.hw1_javaframeworks.DAO.AccountDao;
import lombok.RequiredArgsConstructor;
import com.example.hw1_javaframeworks.models.Account;
import com.example.hw1_javaframeworks.models.Currency;
import com.example.hw1_javaframeworks.models.Customer;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountDao accountDao;


    public void deposit(String accountNumber, double amount) {
        Account account = accountDao.getByAccountNumber(accountNumber);
        if (account != null) {
            account.setBalance(account.getBalance() + amount);
            accountDao.save(account);
        } else {
            throw new IllegalArgumentException("Account not found");
        }
    }

    public void withdraw(String accountNumber, double amount) {
        Account account = accountDao.getByAccountNumber(accountNumber);
        if (account != null && account.getBalance() >= amount) {
            account.setBalance(account.getBalance() - amount);
            accountDao.save(account);
        } else {
            throw new IllegalArgumentException("Insufficient balance");
        }
    }

    public void transfer(String fromAccountNumber, String toAccountNumber, double amount) {
        Account fromAccount = accountDao.getByAccountNumber(fromAccountNumber);
        Account toAccount = accountDao.getByAccountNumber(toAccountNumber);

        if (fromAccount != null && toAccount != null && fromAccount.getBalance() >= amount) {
            fromAccount.setBalance(fromAccount.getBalance() - amount);
            toAccount.setBalance(toAccount.getBalance() + amount);

            accountDao.save(fromAccount);
            accountDao.save(toAccount);
        } else {
            throw new IllegalArgumentException("Invalid accounts or insufficient balance");
        }
    }

    public Account createAccount(Currency currency, Customer customer) {
        Account account = new Account(currency, customer);
        return accountDao.save(account);
    }

    public void saveAccount(Account account) {
        accountDao.save(account);
    }

    public void deleteAccount(Long accountId) {
        accountDao.deleteById(accountId);
    }
}
