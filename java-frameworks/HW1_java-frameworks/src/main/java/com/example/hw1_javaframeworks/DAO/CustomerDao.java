package com.example.hw1_javaframeworks.DAO;

import jakarta.annotation.PostConstruct;
import com.example.hw1_javaframeworks.models.Customer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class CustomerDao implements Dao<Customer> {

    List<Customer> customers = new ArrayList<>();
    private long nextId = 1;

    @PostConstruct
    public void init(){
        this.save(new Customer( "Ivan Shevchenko", "ivanpetrenko@mail.com", 22));
//        this.save(new Customer( "Maria Ivanova", "mariaivanova@mail.com", 30));
//        this.save(new Customer( "Petro Sidorov", "petrosidorov@mail.com", 28));
//        this.save(new Customer( "Anna Kovalenko", "annakovalenko@mail.com", 35));
//        this.save(new Customer( "Oleksandr Hrytsenko", "oleksandrhrytsenko@mail.com", 40));
//        this.save(new Customer( "Yulia Tkachenko", "yuliatkachenko@mail.com", 26));
//        this.save(new Customer( "Andriy Shevchenko", "andriyshevchenko@mail.com", 33));

    }


    @Override
    public Customer save(Customer obj) {
        if (obj.getId() == null) {
            obj.setId(nextId++);
            customers.add(obj);
        } else {
            Customer existingCustomer = getOne(obj.getId());
            if (existingCustomer != null) {
                customers.remove(existingCustomer);
                customers.add(obj);
            } else {
                obj.setId(nextId++);
                customers.add(obj);
            }
        }
        return obj;
    }

    @Override
    public boolean delete(Customer obj) {
        return customers.remove(obj);
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        customers.removeAll(entities);
    }

    @Override
    public void saveAll(List<Customer> entities) {
        for (Customer customer : entities) {
            save(customer);
        }
    }

    @Override
    public List<Customer> findAll() {
        return customers;
    }

    @Override
    public boolean deleteById(long id) {
        Optional<Customer> optionalCustomer = customers.stream().filter(c -> c.getId() == id).findFirst();
        if (optionalCustomer.isPresent()) {
            customers.remove(optionalCustomer.get());
            return true;
        }
        return false;
    }

    @Override
    public Customer getOne(long id) {
        Optional<Customer> optionalCustomer = customers.stream().filter(c -> c.getId() == id).findFirst();
        return optionalCustomer.orElse(null);
    }
}
