package com.example.hw1_javaframeworks.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.util.UUID;
//@Getter
//@Setter
//@ToString(includeFieldNames=true)
//@EqualsAndHashCode
@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class Account {
    private Long id;
    private String number;
    private Currency currency;
    private Double balance = 0.0;
    @JsonIgnore
    private Customer customer;


    public Account(Currency currency, Customer customer) {
        this.number = UUID.randomUUID().toString();
        this.currency = currency;
        this.customer = customer;
    }


}