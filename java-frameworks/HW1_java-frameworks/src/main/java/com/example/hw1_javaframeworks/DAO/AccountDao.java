package com.example.hw1_javaframeworks.DAO;

import com.example.hw1_javaframeworks.models.Account;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class AccountDao implements Dao<Account> {

    private List<Account> accounts = new ArrayList<>();
    private long nextId = 1;

    @Override
    public Account save(Account obj) {
        if (obj.getId() == null) {
            obj.setId(nextId++);
            accounts.add(obj);
        } else {
            Account existingAccount = getOne(obj.getId());
            if (existingAccount != null) {
                accounts.remove(existingAccount);
                accounts.add(obj);
            } else {
                obj.setId(nextId++);
                accounts.add(obj);
            }
        }
        return obj;
    }

    @Override
    public boolean delete(Account obj) {
        if (accounts.contains(obj)) {
            accounts.remove(obj);
            return true;
        }
        return false;
    }

    @Override
    public void deleteAll(List<Account> entities) {
        accounts.removeAll(entities);
    }

    @Override
    public void saveAll(List<Account> entities) {
        for (Account account : entities) {
            save(account);
        }
    }

    @Override
    public List<Account> findAll() {
        return accounts;
    }

    @Override
    public boolean deleteById(long id) {
        Optional<Account> optionalAccount = accounts.stream().filter(a -> a.getId() == id).findFirst();
        if (optionalAccount.isPresent()) {
            accounts.remove(optionalAccount.get());
            return true;
        }
        return false;
    }

    @Override
    public Account getOne(long id) {
        Optional<Account> optionalAccount = accounts.stream().filter(a -> a.getId() == id).findFirst();
        return optionalAccount.orElse(null);
    }
    public Account getByAccountNumber(String number) {
        Optional<Account> optionalAccount = accounts.stream().filter(a -> a.getNumber().equals(number)).findFirst();
        return optionalAccount.orElse(null);
    }
}
