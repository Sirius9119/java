package com.example.hw1_javaframeworks.Controller;

import com.example.hw1_javaframeworks.Service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class AccountController {

    private final AccountService accountService;

    @PutMapping("/deposit/{accountNumber}")
    public void deposit(@PathVariable String accountNumber, @RequestBody double amount) {
        accountService.deposit(accountNumber, amount);
    }

    @PutMapping("/withdraw/{accountNumber}")
    public void withdraw(@PathVariable String accountNumber, @RequestBody double amount) {
        accountService.withdraw(accountNumber, amount);
    }

    @PostMapping("/transfer/{fromAccountNumber}/{toAccountNumber}")
    public void transfer(@PathVariable String fromAccountNumber, @PathVariable String toAccountNumber, @RequestBody double amount) {
        accountService.transfer(fromAccountNumber, toAccountNumber, amount);
    }
}
