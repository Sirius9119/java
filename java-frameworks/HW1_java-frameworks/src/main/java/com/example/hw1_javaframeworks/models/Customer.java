package com.example.hw1_javaframeworks.models;

import java.util.ArrayList;
import java.util.List;

import lombok.*;

//@Getter
//@Setter
//@ToString(includeFieldNames=true)
//@EqualsAndHashCode
@Data
@AllArgsConstructor
@RequiredArgsConstructor

public class Customer {
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private List<Account> accounts = new ArrayList<>();


    public Customer(String name, String email, Integer age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }


}
