package com.example.hw1_javaframeworks.Service;

import com.example.hw1_javaframeworks.DAO.CustomerDao;
import lombok.RequiredArgsConstructor;
import com.example.hw1_javaframeworks.models.Account;
import com.example.hw1_javaframeworks.models.Currency;
import com.example.hw1_javaframeworks.models.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerDao customerDao;
    private final AccountService accountService;



    public Customer createCustomer(Customer customer) {
        return customerDao.save(customer);
    }


    public Customer getCustomerById(long id) {
        return customerDao.getOne(id);
    }

    public List<Customer> getAllCustomers() {
        return customerDao.findAll();
    }

    public Customer updateCustomer(long id, String name, String email, int age) {
        Customer customer = customerDao.getOne(id);
        if (customer != null) {
            customer.setName(name);
            customer.setEmail(email);
            customer.setAge(age);
           return customerDao.save(customer);
        } else {
            throw new IllegalArgumentException("Customer not found");
        }
    }



    public void deleteCustomer(long id) {
        customerDao.deleteById(id);
    }

    public void createAccount(Long id, Currency currency) {
        Customer customerById = customerDao.getOne(id);
        if (customerById != null) {
            Account account = accountService.createAccount(currency, customerById);
            customerById.getAccounts().add(account);
            customerDao.save(customerById);
        } else {
            throw new IllegalArgumentException("Customer not found");
        }
    }

    public void deleteAccount(Long id, Long accountId) {
        Customer customer = customerDao.getOne(id);
        if (customer != null) {
            Account accountToRemove = customer.getAccounts().stream()
                    .filter(account -> account.getId().equals(accountId))
                    .findFirst()
                    .orElse(null);
            if (accountToRemove != null) {
                customer.getAccounts().remove(accountToRemove);
                customerDao.save(customer);
                accountService.deleteAccount(accountId);
            }
        }
    }}
