import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class PetTest {
    @Test
    void test_toString_without_habits() {
        Pet dog = new Pet(Species.DOG, "Bobik", 5, 85, new String[]{"eat", "drink", "sleep"});
        String expected = "DOG{nickname='Bobik', age=5, trickLevel=85, habits=[eat, drink, sleep]}";

        assertEquals(expected, dog.toString());
    }

}
