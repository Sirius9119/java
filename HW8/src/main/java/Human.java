import java.util.*;


class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Family family;
    private Map<String, String> schedule;


    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, Human mother, Human father, Pet pet, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = (schedule != null) ? schedule : new HashMap<>();
    }

    public Human() {
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }
    public Family getFamily(){ return family; }

    public Map<String, String> getSchedule() {
        return schedule;
    }
    //Сэттэры
    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }
    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public void setFamily(Family family) { this.family = family; }



    public void greetPet() {
        Set<Pet> familyPets = this.family.getPets();
        if (familyPets != null && !familyPets.isEmpty()) {
            Pet humanPet = familyPets.iterator().next();
            System.out.println("Hello, " + humanPet.getNickname());
        } else {
            System.out.println("I don't have a pet.");
        }
    }
    public String getGender() {
        List<String> genders = Arrays.asList("male", "female");
        Random random = new Random();
        return genders.get(random.nextInt(genders.size()));
    }
    private int calculateAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear - birthYear;
    }

    public int getAge() {
        return calculateAge(year);
    }


    public void describePet() {
        Set<Pet> familyPets = this.family.getPets();
        if (familyPets != null && !familyPets.isEmpty()) {
            Pet humanPet = familyPets.iterator().next();
            System.out.println("I have a " + humanPet.getSpecies() + ", " + humanPet.getNickname() +
                    ", who is " + (humanPet.getTrickLevel() > 50 ? "very clever" : "not very clever") + ".");
        } else {
            System.out.println("I don't have a pet.");
        }
    }


    @Override
    public String toString() {
        return "Human{" +
                "name='" + this.name + '\'' +
                ", surname='" + this.surname + '\'' +
                ", year=" + this.year +
                ", iq=" + this.iq +
                ", pet=" + this.pet +
                ", schedule=" + scheduleToString() +
                '}';
    }

    private String scheduleToString() {
        if (schedule == null) {
            return "{}";
        }

        StringBuilder result = new StringBuilder("{");
        schedule.forEach((key, value) -> result.append(key).append("=").append(value).append(", "));
        if (!schedule.isEmpty()) {
            result.setLength(result.length() - 2);
        }
        result.append("}");
        return result.toString();
    }
//    @Override
//    protected void finalize() throws Throwable {
//        System.out.println("Finalizing Human: " + this);
//        super.finalize();
//    }
}
final class Man extends Human {
    @Override
    public void greetPet() {
        System.out.println("Hey there, little buddy!");
    }


    public void repairCar() {
        System.out.println("Repairing the car...");
    }
}

final class Woman extends Human {


    @Override
    public void greetPet() {
        System.out.println("Hello, sweetie!");
    }

    public void makeup() {
        System.out.println("Applying makeup...");
    }
}