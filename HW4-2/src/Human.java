import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    //    private Human mother; // Можливо це
//    private Human father; // Можливо це
    private Family family;
    private String[][] schedule;
    private Human() {
    }
    public Human(String name, String surname, int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year, Pet pet, Human mother, Human father){
        this.name = name;
        this.surname = surname;
        this.year = year;
//        this.mother = mother;
//        this.father = father;
    }
    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
//        this.pet = pet;
//        this.mother = mother;
//        this.father = father;
        this.schedule = schedule;
    }
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

//    public Pet getPet() {
//        return pet;
//    }

//    public Human getMother() {
//        return mother;
//    }

//    public Human getFather() {
//        return father;
//    }

    public String[][] getSchedule() {
        return schedule;
    }
    public Family getFamily(){
        return family;
    }

    // Сеттери
    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

//    public void setPet(Pet pet) {
//        this.pet = pet;
//    }

//    public void setMother(Object mother) {
//        this.mother = (Human) mother;
//    }

//    public void setFather(Object father) {
//        this.father = (Human) father;
//    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public void setFamily(Family family) { this.family = family; }

    public void greetPet(){
        System.out.println(String.format("Привіт, %s", this.family.getPet().getNickname()));
    }
    public void describePet(){
        System.out.println(String.format("У мене є %s, їй %d років, він %s", this.family.getPet().getSpecies(), this.family.getPet().getAge(), this.family.getPet().getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий"));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human human)) return false;
        return year == human.year && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(pet, human.pet) && Arrays.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq, pet);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }

    @Override
    public String toString() {
        String s = "Human{" +
                "name='" + this.name + '\'' +
                ", surname='" + this.surname + '\'' +
                ", year=" + this.year +
                (this.iq == 0 ? "" : (", iq=" +  this.iq)) +
//                ", mother=" + (this.mother == null ? "Undefined": (this.mother.getName() + " " + this.mother.getSurname())) +
//                ", father=" + (this.father == null ? "Undefined": (this.father.getName() + " " + this.father.getSurname())) +
                (this.pet == null ? "" : (", pet=" + this.pet)) +
                '}';
        return s;
    }

    public boolean feedPet(boolean feedingTime, Pet pet) {
        Random rnd = new Random();
        int rndTrickLevel = rnd.nextInt(101);
        if (feedingTime || rndTrickLevel < pet.getTrickLevel()) {
            System.out.println(String.format("Хм... годувати я %s", pet.getNickname()));
            return true;
        } else {
            System.out.println(String.format("Думаю, %s не голодний.", pet.getNickname()));
            return false;
        }

    }
}
