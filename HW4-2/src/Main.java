public class Main {
    public static void main(String[] args) {
        Pet dog = new Pet("dog", "Rock", 5, 75, new String[]{"eat", "drink", "sleep"});
        Human mother = new Human("Jane", "Karleone", 1951);
        Human father = new Human("Vito", "Karleone", 1949);
        Human michaelKarleone = new Human("Michael", "Karleone", 1977, 90, dog, mother, father, new String[][]{{"day", "task"}, {"day_2", "task_2"}});
        System.out.println(dog.toString());
        System.out.println(michaelKarleone.toString());
        Family family1 = new Family(mother, father);
        System.out.println(family1.toString());
//                michaelKarleone.greetPet();
//                michaelKarleone.describePet();
        family1.addChild(michaelKarleone);
        System.out.println("Quantity of family: " + family1.countFamily());
        family1.setPet(dog);
        System.out.println(family1.toString());
        family1.getMother().greetPet();

        boolean timeForFeed = false;
        System.out.println("Тварина нагодована: " + mother.feedPet(timeForFeed, dog));
    }
}
