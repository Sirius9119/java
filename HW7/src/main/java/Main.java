import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        Dog dog = new Dog("Sharik", 5, 85, new HashSet<>(Set.of("eat", "drink", "sleep")));
        DomesticCat cat = new DomesticCat("Murchik", 5, 85, new HashSet<>(Set.of("sleep","eat")));
        Human mother = new Human("Jane", "Doe", 1980);
        Human father = new Human("John", "Doe", 1978);
        Human child = new Human("Alice", "Doe", 2005);

        Map<String, String> scheduleChild = Map.of(
                DayOfWeek.MONDAY.name().toLowerCase(), DayOfWeek.MONDAY.getToDo(),
                DayOfWeek.THURSDAY.name().toLowerCase(), DayOfWeek.THURSDAY.getToDo(),
                DayOfWeek.SATURDAY.name().toLowerCase(), DayOfWeek.SATURDAY.getToDo()
        );


        Human familyChild = new Human("Bob", "Doe", 2010, 120, mother, father, dog, scheduleChild );

        Family family = new Family(mother, father);
        family.addChild(child);
        family.addPet(dog);
        family.addPet(cat);
        dog.foul();



        System.out.println(dog);
        System.out.println(mother);
        System.out.println(father);
        System.out.println(child);
        System.out.println(familyChild);
        System.out.println(family);

//        for (int i = 0; i < 1_000_000; i++) {
//            new Human("John", "Doe", 1980 + i);
//        }
    }
}