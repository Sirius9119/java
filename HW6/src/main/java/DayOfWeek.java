public enum DayOfWeek {
        MONDAY("Start working week"),
        TUESDAY("Running day"),
        WEDNESDAY("Gym day"),
        THURSDAY("Meeting"),
        FRIDAY("IT day"),
        SATURDAY("Family day"),
        SUNDAY("Day for relax");
        private String toDo;
        DayOfWeek(String toDo) {
                this.toDo = toDo;
        }

        public String getToDo() {
                return toDo;
        }
}

