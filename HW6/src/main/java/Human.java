import java.util.Arrays;



class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Family family;
    private String[][] schedule;

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, Human mother, Human father, Pet pet, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human() {
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }
    public Family getFamily(){ return family; }

    public String[][] getSchedule() {
        return schedule;
    }
    //Сэттэры
    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public void setFamily(Family family) { this.family = family; }



    public void greetPet() {
        Pet humanPet = this.family.getPet();
        if (humanPet != null) {
            System.out.println("Hello, " + humanPet.getNickname());
        } else {
            System.out.println("I don't have a pet.");
        }
    }

    public void describePet() {
        Pet humanPet = this.family.getPet();
        if (humanPet != null) {
            System.out.println("I have a " + humanPet.getSpecies() + ", " + humanPet.getNickname() +
                    ", who is " + (humanPet.getTrickLevel() > 50 ? "very clever" : "not very clever") + ".");
        } else {
            System.out.println("I don't have a pet.");
        }
    }


    @Override
    public String toString() {
        return "Human{" +
                "name='" + this.name + '\'' +
                ", surname='" + this.surname + '\'' +
                ", year=" + this.year +
                ", iq=" + this.iq +
                ", pet=" + this.pet +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }
//    @Override
//    protected void finalize() throws Throwable {
//        System.out.println("Finalizing Human: " + this);
//        super.finalize();
//    }
}
final class Man extends Human {
    @Override
    public void greetPet() {
        System.out.println("Hey there, little buddy!");
    }


    public void repairCar() {
        System.out.println("Repairing the car...");
    }
}

final class Woman extends Human {


    @Override
    public void greetPet() {
        System.out.println("Hello, sweetie!");
    }

    public void makeup() {
        System.out.println("Applying makeup...");
    }
}