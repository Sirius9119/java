import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int secretNumber = random.nextInt(101);
        System.out.println("Начнём игру");
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите свое имя: ");
        String name = scanner.nextLine();
        while (true) {
            System.out.print("Введите число: ");
            int guess = scanner.nextInt();
            if (guess < secretNumber) {
                System.out.println("Ваше число меньше. Введите еще.");
            } else if (guess > secretNumber) {
                System.out.println("Ваше число больше. Введите еще.");
            } else {
                System.out.println("Поздраляем, " + name + "!");
                break;
            }
        }
        scanner.close();
    }
}
