import java.util.Random;
import java.util.Scanner;

public class ShootingGame {
    public static void main(String[] args) {
        Random random = new Random();

        int size = 5;

        char[][] gameBoard = new char[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                gameBoard[i][j] = '-';
            }
        }

        int targetRow = random.nextInt(size);
        int targetCol = random.nextInt(size);

        System.out.println("All Set. Get ready to rumble!");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            printGameBoard(gameBoard);

            System.out.print("Enter row (1-5): ");
            int playerRow = scanner.nextInt() - 1;

            if (playerRow < 0 || playerRow >= size) {
                System.out.println("Invalid row. Please, try again.");
                continue;
            }

            System.out.print("Enter column (1-5): ");
            int playerCol = scanner.nextInt() - 1;

            if (playerCol < 0 || playerCol >= size) {
                System.out.println("Invalid column. Please, try again.");
                continue;
            }

            if (playerRow == targetRow && playerCol == targetCol) {
                gameBoard[targetRow][targetCol] = 'x';
                printGameBoard(gameBoard);
                System.out.println("You have won!");
                break;
            } else {
                gameBoard[playerRow][playerCol] = '*';
            }
        }

        scanner.close();
    }

    private static void printGameBoard(char[][] gameBoard) {
        System.out.println("  | 1 | 2 | 3 | 4 | 5 |");
        for (int i = 0; i < gameBoard.length; i++) {
            System.out.print((i + 1) + " | ");
            for (int j = 0; j < gameBoard[i].length; j++) {
                System.out.print(gameBoard[i][j] + " | ");
            }
            System.out.println();
        }
    }
}
