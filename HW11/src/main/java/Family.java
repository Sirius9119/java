import java.util.*;
import java.util.stream.Collectors;

class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
        this.children = new ArrayList<>(List.of());
        this.pets = new HashSet<>();
    }

    public void addChild(Human child) {
        children.add(child);
        child.setFamily(this);
    }

    public boolean deleteChild(Human child) {
        return children.remove(child);
    }

    public boolean deleteChild(int index) {
        if (index >= 0 && index < children.size()) {
            Human removedChild = children.remove(index);
            removedChild.setFamily(null);
            return true;
        }
        return false;
    }

    public void addPet(Pet pet) {
        pets.add(pet);
    }

    public boolean removePet(Pet pet) {
        return pets.remove(pet);
    }

    public int countFamily() {
        return 2 + children.size(); // 2 parents + number of children
    }

    public Family(Set<Pet> pets) {
        this.pets = pets;
    }



    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    //Сэттэры
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public void setChildren(List<Human> children) {this.children = children;}
    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    public String prettyFormat() {
        StringBuilder result = new StringBuilder("family:");

        result.append("\n\tmother: ").append(mother.prettyFormat());
        result.append("\n\tfather: ").append(father.prettyFormat());

        if (!children.isEmpty()) {
            result.append("\n\tchildren:");
            for (Human child : children) {
                result.append("\n\t\t").append(child.prettyFormat());
            }
        }

        if (!pets.isEmpty()) {
            result.append("\n\tpets: ").append(pets.stream()
                    .map(Pet::prettyFormat)
                    .collect(Collectors.toList()));
        }

        return result.toString();
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pets=" + pets +
                '}';
    }
//    @Override
//    protected void finalize() throws Throwable {
//        System.out.println("Finalizing Family: " + this);
//        super.finalize();
//    }

}