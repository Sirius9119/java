import java.time.LocalDate;
import java.time.ZoneOffset;

public class UnixTimeConverter {
    public static long convertToUnixTime(int year, int month, int day) {
        LocalDate localDate = LocalDate.of(year, month, day);
        return localDate.atStartOfDay(ZoneOffset.UTC).toEpochSecond();
    }


}