import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;


class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Pet pet;
    private Family family;
    private Map<String, String> schedule;


    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }
    public Human(String name, String surname, int year, int month, int day, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = UnixTimeConverter.convertToUnixTime(year, month, day);
        this.iq = iq;

    }


    public Human(String name, String surname, long birthDate, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human(String name, String surname, long birthDate, int iq, Human mother, Human father, Pet pet, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = (schedule != null) ? schedule : new HashMap<>();
    }
    //конструктор для усиновлених дітей
    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = UnixTimeConverter.convertToUnixTime(year, 1, 1);
        this.iq = iq;
    }

    public Human() {
    }



    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public int getIq() {
        return iq;
    }
    public Family getFamily(){ return family; }

    public Map<String, String> getSchedule() {
        return schedule;
    }
    //Сэттэры
    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public void setFamily(Family family) { this.family = family; }



    public void greetPet() {
        Set<Pet> familyPets = this.family.getPets();
        if (familyPets != null && !familyPets.isEmpty()) {
            Pet humanPet = familyPets.iterator().next();
            System.out.println("Hello, " + humanPet.getNickname());
        } else {
            System.out.println("I don't have a pet.");
        }
    }
    public String getGender() {
        List<String> genders = Arrays.asList("male", "female");
        Random random = new Random();
        return genders.get(random.nextInt(genders.size()));
    }
    public int getAge() {
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        long birthTimeSeconds = birthDate;

        long difference = currentTimeMillis - birthTimeSeconds;
        long secondsInYear = 31536000;

        int age = (int) (difference / secondsInYear);

        return age;
    }
    public String describeAge() {
        LocalDate birthLocalDate = LocalDate.ofEpochDay(birthDate / (24 * 60 * 60 * 1000));

        LocalDate currentDate = LocalDate.now();
        Period agePeriod = Period.between(birthLocalDate, currentDate);

        int years = agePeriod.getYears();
        int months = agePeriod.getMonths();
        int days = agePeriod.getDays();

        return String.format("Person's age: %d years, %d months, and %d days.", years, months, days);
    }


    public void describePet() {
        Set<Pet> familyPets = this.family.getPets();
        if (familyPets != null && !familyPets.isEmpty()) {
            Pet humanPet = familyPets.iterator().next();
            System.out.println("I have a " + humanPet.getSpecies() + ", " + humanPet.getNickname() +
                    ", who is " + (humanPet.getTrickLevel() > 50 ? "very clever" : "not very clever") + ".");
        } else {
            System.out.println("I don't have a pet.");
        }
    }
    public String prettyFormat() {
        LocalDateTime dateTime = Instant.ofEpochSecond(birthDate).atZone(ZoneId.systemDefault()).toLocalDateTime();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String formattedDate = dateTime.format(formatter);
        return String.format("{name='%s', surname='%s', birthDate='%s', iq=%d, schedule=%s}",
                name, surname, formattedDate, iq, schedule);
    }


    @Override
    public String toString() {
       LocalDate localDate = Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String formattedBirthDate = localDate.format(formatter);
        return "Human{" +
                "name='" + this.name + '\'' +
                ", surname='" + this.surname + '\'' +
                ", birthDate=" + formattedBirthDate +
                ", iq=" + this.iq +
                ", pet=" + this.pet +
                ", schedule=" + scheduleToString() +
                '}';
    }

    private String scheduleToString() {
        if (schedule == null) {
            return "{}";
        }

        StringBuilder result = new StringBuilder("{");
        schedule.forEach((key, value) -> result.append(key).append("=").append(value).append(", "));
        if (!schedule.isEmpty()) {
            result.setLength(result.length() - 2);
        }
        result.append("}");
        return result.toString();
    }
//    @Override
//    protected void finalize() throws Throwable {
//        System.out.println("Finalizing Human: " + this);
//        super.finalize();
//    }
}
final class Man extends Human {
    @Override
    public void greetPet() {
        System.out.println("Hey there, little buddy!");
    }


    public void repairCar() {
        System.out.println("Repairing the car...");
    }
}

final class Woman extends Human {


    @Override
    public void greetPet() {
        System.out.println("Hello, sweetie!");
    }

    public void makeup() {
        System.out.println("Applying makeup...");
    }
}