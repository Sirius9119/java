public enum Species {
    DOG("Dog"),
    DOMESTICCAT("Domestic Cat"),
    FISH("Fish"),
    ROBOCAT("Robotic Cat"),
    UNKNOWN("Unknown");

    private final String displayValue;

    Species(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}