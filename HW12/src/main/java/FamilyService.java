import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public void fillWithTestData() {
        int numberOfFamily = ThreadLocalRandom.current().nextInt(3, 6);
        for (int i = 0; i < numberOfFamily; i++) {
            Human mother = new Human("Мати" + (i + 1), "Прізвище" + (i + 1), 1980 + i, 1, 1, 120);
            Human father = new Human("Батько" + (i + 1), "Прізвище" + (i + 1), 1978 + i, 3, 15, 130);

            Family family = createNewFamily(mother, father);
            int numberOfChildren = ThreadLocalRandom.current().nextInt(0, 4);

            for (int j = 0; j < numberOfChildren; j++) {
                int randomYear = ThreadLocalRandom.current().nextInt(2005, 2024);

                Human child = (j % 2 == 0) ? new Human("Alex" + (j + 1), "Doe", randomYear, 1, 23, 120) :
                        new Human("Дівчинка" + (j + 1), "Doe", randomYear, 1, 23, 120);

                adoptChild(family, child);
            }

        }

    }

    public List<Family> loadFromFile(){return familyDao.loadFromFile();}

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }


    public void displayFamily(int index) {
        Family family = familyDao.getFamilyByIndex(index);
        if (family != null) {
            System.out.println(family.prettyFormat());
        } else {
            System.out.println("Family not found.");
        }
    }

    public void displayAllFamilies() {
        List<Family> families = familyDao.getAllFamilies();
        int i = 1;
        if (families.size() == 0) {
            System.out.println("No families found");
        }

        for (Family family : families) {
            System.out.println("Family #" + i++ + ":");
            System.out.println(family.prettyFormat());
        }
    }



    public List<Family> getFamiliesBiggerThan(int count) {
        return familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() > count)
                .collect(Collectors.toList());
    }


    public List<Family> getFamiliesLessThan(int count) {
        return familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() < count)
                .collect(Collectors.toList());
    }


    public long countFamiliesWithMemberNumber(int numberOfPeople) {
        return familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() == numberOfPeople)
                .count();
    }


    public Family createNewFamily(Human mother, Human father) {
        Family newFamily = new Family(mother, father);
        familyDao.saveFamily(newFamily);
        return newFamily;
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String maleName, String femaleName) {
        Human child = new Human();
        String gender = child.getGender();
        child.setName(gender.equals("male") ? maleName : femaleName);
        child.setSurname(gender.equals("male") ? family.getFather().getSurname() : family.getMother().getSurname());
        child.setBirthDate(Instant.now().getEpochSecond());
        child.setFamily(family);
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThan(int age) {
        List<Family> families = familyDao.getAllFamilies();
        families.forEach(family ->
                family.getChildren().removeIf(child -> child.getAge() > age)

        );
        familyDao.saveData(families);
    }


    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public List<Pet> getPets(int index) {
        Family family = familyDao.getFamilyByIndex(index);
        return family != null ? family.getPets().stream().toList() : null;
    }

    public void addPet(int index, Pet pet) {
        Family family = familyDao.getFamilyByIndex(index);
        if (family != null) {
            family.addPet(pet);
            familyDao.saveFamily(family);
        }
    }
}
