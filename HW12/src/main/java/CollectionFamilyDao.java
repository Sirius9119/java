import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families = new ArrayList<>();
    private static final String FILE_NAME = "families.dat";


        @Override
    public List<Family> getAllFamilies() {
        return new ArrayList<>(families);
    }
    @Override
    public List<Family> loadFromFile() {
        try {
            File file = new File(FILE_NAME);
            if (!file.exists()) {
                file.createNewFile();
                families = new ArrayList<>(); // обновляем список families
                return families;
            }
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                families = (List<Family>) ois.readObject(); // обновляем список families
                return families;
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    public Family getFamilyByIndex(int index) {
        List<Family> families = getAllFamilies();
        if (families != null && index >= 0 && index < families.size()) {
            return families.get(index);
        }
        return null;
    }


    @Override
    public boolean deleteFamily(int index) {
        List<Family> families = getAllFamilies();
        if (families != null && index >= 0 && index < families.size()) {
            families.remove(index);
            saveData(families);
            return true;
        }
        return false;
    }


    @Override
    public boolean deleteFamily(Family family) {
        List<Family> families = getAllFamilies();
        if (families != null && families.contains(family)) {
            families.remove(family);
            saveData(families);
            return true;
        }
        return false;
    }



    @Override
    public boolean saveFamily(Family family) {
        List<Family> families = getAllFamilies();
        if (families == null) {
            families = new ArrayList<>();
        }
        if (families.contains(family)) {
            System.out.println(family + " если есть семья");
            int index = families.indexOf(family);
            families.set(index, family);
        } else {
            System.out.println(families + "all families");
            families.add(family);
            System.out.println(family + "если нет семьи");
        }
        saveData(families);
        return true;
    }

    public void saveData(List<Family> families) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FILE_NAME))) {
            oos.writeObject(families);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
