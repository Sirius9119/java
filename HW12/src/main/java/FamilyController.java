import java.time.Month;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class FamilyController {
    int MAX_FAMILY_SIZE = 6;
    private FamilyService familyService;


    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public void fillWithTestData() {
        familyService.fillWithTestData();
    }
    public List<Family> loadFromFile() {
        return familyService.loadFromFile();
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int numberOfPeople) {
        return familyService.getFamiliesBiggerThan(numberOfPeople);
    }

    public List<Family> getFamiliesLessThan(int numberOfPeople) {
        return familyService.getFamiliesLessThan(numberOfPeople);
    }

    public int countFamiliesWithMemberNumber(int numberOfPeople) {
        return (int) familyService.countFamiliesWithMemberNumber(numberOfPeople);
    }

    public Family createNewFamily(Human mother, Human father) {
        return familyService.createNewFamily(mother, father);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String maleName, String femaleName) {
        if (family.countFamily() >= MAX_FAMILY_SIZE) {
            throw new FamilyOverflowException();
        }
        return familyService.bornChild(family, maleName, femaleName);
    }

    public Family adoptChild(Family family, Human child) {
        if (family.countFamily() >= MAX_FAMILY_SIZE) {
            throw new FamilyOverflowException();
        }
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyService.deleteAllChildrenOlderThan(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public List<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet pet) {
        familyService.addPet(index, pet);
    }

    public void console() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            DisplayMenu.displayMenu();

            int command = scanner.nextInt();
            switch (command) {
                case 1:
//                    fillWithTestData();
                    loadFromFile();

                    break;
                case 2:
                    displayAllFamilies();
                    break;
                case 3:
                    System.out.println("Задайте кількость людей");
                    int biggerThan = scanner.nextInt();
                    List<Family> familiesBigger = getFamiliesBiggerThan(biggerThan);
                    if (familiesBigger.size() < biggerThan) {
                        System.out.println("Немаэ сiмей з обраной кiлькостi людей");
                    }
                    int i = 1;
                    for (Family family : familiesBigger) {
                        System.out.println("Family #" + i++ + ":");
                        System.out.println(family.prettyFormat());
                    }
                    break;
                case 4:
                    System.out.println("Задайте кількость людей");
                    int lessThan  = scanner.nextInt();
                    List<Family> familiesLess = getFamiliesLessThan(lessThan );
                    if (familiesLess.size() < lessThan ) {
                        System.out.println("Немаэ сiмей з обраной кiлькостi людей");
                    }
                    int j = 1;
                    for (Family family : familiesLess) {
                        System.out.println("Family #" + j++ + ":");
                        System.out.println(family.prettyFormat());
                    }
                    break;
                case 5:
                    System.out.println("Задайте кількость людей");
                    int count  = scanner.nextInt();
                    System.out.println(countFamiliesWithMemberNumber(count) + " ciмей з обраной кiлькостi людей");
                    break;
                case 6:
                    System.out.println("ім'я матері");
                    String motherName = scanner.next();
                    System.out.println("прізвище матері");
                    String motherSurname = scanner.next();
                    System.out.println("рік народження матері");
                    int motherBornYear = scanner.nextInt();
                    System.out.println("місяць народження матері");
                    int motherBornMonth = scanner.nextInt();
                    System.out.println("день народження матері");
                    int motherBornDay = scanner.nextInt();
                    System.out.println("iq матері");
                    int motherIq = scanner.nextInt();
                    Human mother = new Human(motherName, motherSurname, motherBornYear, motherBornMonth, motherBornDay, motherIq);
                    System.out.println("ім'я батька");
                    String fatherName = scanner.next();
                    System.out.println("прізвище батька");
                    String fatherSurname = scanner.next();
                    System.out.println("рік народження батька");
                    int fatherBornYear = scanner.nextInt();
                    System.out.println("місяць народження батька");
                    int fatherBornMonth = scanner.nextInt();
                    System.out.println("день народження батька");
                    int fatherBornDay = scanner.nextInt();
                    System.out.println("iq батька");
                    int fatherIq = scanner.nextInt();
                    Human father = new Human(fatherName, fatherSurname, fatherBornYear, fatherBornMonth, fatherBornDay, fatherIq);
                    System.out.println(createNewFamily(mother, father).prettyFormat());
                    break;
                case 7:
                    System.out.println("Задайте індекс сім'ї");
                    int deleteIndex = scanner.nextInt();
                    if (deleteFamilyByIndex(--deleteIndex)) {
                        System.out.println("Сім'я видалена");
                    } else {
                        System.out.println(" Невірний індекс сім'ї ");
                    }
                    break;
                case 8:
                    boolean exitSubMenu = false;
                    while (!exitSubMenu) {
                        DisplayMenu.displaySubmenuParagraph8();
                        int com = scanner.nextInt();
                        switch (com) {
                            case 1:
                                System.out.println("Введить індекс сім'ї");
                                int bornFamily = scanner.nextInt();
                                System.out.println("Введить ім'я для хлопчика");
                                String maleName = scanner.next();
                                System.out.println("Введить ім'я для дівчинки");
                                String femaleName = scanner.next();
                                System.out.println(bornChild(getFamilyById(--bornFamily),maleName,femaleName).prettyFormat());
                                break;
                            case 2:
                                System.out.println("Введить індекс сім'ї");
                                int adoptFamily = scanner.nextInt();
                                System.out.println("Введить ім'я");
                                String adoptName = scanner.next();
                                System.out.println("Введить прізвище");
                                String adoptSurname = scanner.next();
                                System.out.println("Введить рік народження");
                                int adoptYear = scanner.nextInt();
                                System.out.println("Введить iq");
                                int adoptIq = scanner.nextInt();
                                Human adoptedChild = new Human(adoptName, adoptSurname, adoptYear, adoptIq);
                                System.out.println(adoptChild(getFamilyById(--adoptFamily),adoptedChild).prettyFormat());
                                break;
                            case 3:
                                exitSubMenu = true;
                                break;
                        }
                    }
                    break;
                case 9:
                    System.out.println("Введить вік дітей");
                    int ageToDelete = scanner.nextInt();
                    deleteAllChildrenOlderThan(ageToDelete);
                    break;
                case 0:
                    System.out.println("До побачення!");
                    System.exit(0);
                    scanner.close();
                default:
                    System.out.println(command);
                    System.out.println("Невідома команда. Спробуйте ще раз.");
            }
        }
    }
}
