class DisplayMenu {
    public static void displayMenu() {
        System.out.println("Оберіть опцію:");
        System.out.println("1. Загрузить данный и списка");
        System.out.println("2. Відобразити весь список сімей");
        System.out.println("3. Відобразити список сімей, де кількість людей більша за задану");
        System.out.println("4. Відобразити список сімей, де кількість людей менша за задану");
        System.out.println("5. Підрахувати кількість сімей, де кількість членів дорівнює");
        System.out.println("6. Створити нову родину");
        System.out.println("7. Видалити сім'ю за індексом сім'ї у загальному списку");
        System.out.println("8. Редагувати сім'ю за індексом сім'ї у загальному списку");
        System.out.println("9. Видалити всіх дітей старше віку (у всіх сім'ях видаляються діти старше зазначеного віку - вважатимемо, що вони виросли)");
        System.out.println("0. Вийти и сохранить именения в файл");
    }

    public static void displaySubmenuParagraph8(){
        System.out.println("1. Народити дитину");
        System.out.println("2. Усиновити дитину");
        System.out.println("3. Повернутися до головного меню");
    }

}
