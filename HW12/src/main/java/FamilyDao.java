import java.util.List;

public interface FamilyDao {
    List<Family> loadFromFile();
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamily(int index);
    boolean deleteFamily(Family family);
    boolean saveFamily(Family family);
    void saveData(List<Family> families);
}
