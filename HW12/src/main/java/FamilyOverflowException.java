public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException() {
        super("Family size exceeds the limit.");
    }

    public FamilyOverflowException(String message) {
        super(message);
    }
}
