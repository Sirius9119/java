import java.util.Arrays;
import java.util.Set;


public abstract class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;



    public Pet(String nickname) {
        this.species = Species.UNKNOWN;
        this.nickname = nickname;
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.species = Species.UNKNOWN;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        this.species = (species != null) ? species : Species.UNKNOWN;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet() {
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }
    // Сэттэры
    public void setSpecies(Species species) {
        this.species = species;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }
    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public void eat(){
        System.out.println("I'm eating!");
    }

    public abstract void respond();


    @Override
    public String toString() {
        return species + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + String.join(", ", habits) +
                '}';
    }
//    @Override
//    protected void finalize() throws Throwable {
//        System.out.println("Finalizing Pet: " + this);
//        super.finalize();
//    }
}
interface Fouling {
    void foul();
}

class Dog extends Pet implements Fouling {
    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(Species.DOG, nickname, age, trickLevel, habits);
    }

    public void respond(){
        System.out.println("Hello, owner. I'm " + this.getNickname() + ". I missed you!");
    }
    @Override
    public void foul() {
        System.out.println("I need to cover my tracks...");

    }
}

class DomesticCat extends Pet implements Fouling {
    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(Species.DOMESTICCAT, nickname, age, trickLevel, habits);
    }

    public void respond(){
        System.out.println("Hello, owner. I'm " + this.getNickname() + ". I missed you!");
    }
    @Override
    public void foul() {
        System.out.println("I need to cover my tracks...");

    }
}

class Fish extends Pet {
    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(Species.FISH, nickname, age, trickLevel, habits);
    }

    public void respond(){
        System.out.println("Hello, owner. I'm " + this.getNickname() + ". I missed you!");
    }
}

class RoboCat extends Pet implements Fouling {
    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(Species.ROBOCAT, nickname, age, trickLevel, habits);
    }

    public void respond(){
        System.out.println("Hello, owner. I'm " + this.getNickname() + ". I missed you!");
    }
    @Override
    public void foul() {
        System.out.println("I need to cover my tracks...");

    }
}