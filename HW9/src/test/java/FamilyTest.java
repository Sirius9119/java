import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class FamilyTest {
    @Test
    public void testToString() {
        Human mother = new Human("Jane", "Doe", 1980);
        Human father = new Human("John", "Doe", 1978);
        Family family = new Family(mother, father);

        String expected = "Family{mother=Human{name='Jane', surname='Doe', year=1980, iq=0, pet=null, schedule={}}, father=Human{name='John', surname='Doe', year=1978, iq=0, pet=null, schedule={}}, children=[], pets=[]}";
        assertEquals(expected, family.toString());
    }
    @Test
    public void testDeleteChildByObject() {
        Human mother = new Human("Jane", "Doe", 1980);
        Human father = new Human("John", "Doe", 1978);
        Family family = new Family(mother, father);
        Human child = new Human("Alice", "Doe", 2005);
        family.addChild(child);

        assertTrue(family.deleteChild(child));
        assertEquals(0, family.getChildren().size());
    }

    @Test
    public void testDeleteChildByObjectNotInArray() {
        Human mother = new Human("Jane", "Doe", 1980);
        Human father = new Human("John", "Doe", 1978);
        Family family = new Family(mother, father);
        Human child = new Human("Alice", "Doe", 2005);

        assertFalse(family.deleteChild(child));
        assertEquals(0, family.getChildren().size());
    }

    @Test
    public void testDeleteChildByIndex() {
        Human mother = new Human("Jane", "Doe", 1980);
        Human father = new Human("John", "Doe", 1978);
        Family family = new Family(mother, father);
        Human child = new Human("Alice", "Doe", 2005);
        Human child1 = new Human("Kate", "Doe", 1990);
        family.addChild(child);
        family.addChild(child1);

        assertTrue(family.deleteChild(0));
        assertFalse(family.getChildren().contains(child));
        assertArrayEquals(new Human[]{child1}, family.getChildren().toArray(new Human[0]));
    }

    @Test
    public void testDeleteChildByIndexOutOfRange() {
        Human mother = new Human("Jane", "Doe", 1980);
        Human father = new Human("John", "Doe", 1978);
        Family family = new Family(mother, father);
        Human child = new Human("Alice", "Doe", 2005);
        Human child1 = new Human("Kate", "Doe", 1990);

        family.addChild(child);
        family.addChild(child1);

        assertFalse(family.deleteChild(2));
        assertArrayEquals(new Human[]{child, child1}, family.getChildren().toArray(new Human[0]));
    }

    @Test
    public void testAddChild() {
        Human mother = new Human("Jane", "Doe", 1980);
        Human father = new Human("John", "Doe", 1978);
        Family family = new Family(mother, father);
        Human child = new Human("Alice", "Doe", 2005);

        assertEquals(0, family.getChildren().size());
        family.addChild(child);

        assertEquals(1, family.getChildren().size());
        assertSame(child, family.getChildren().get(0));
        assertSame(family, child.getFamily());
    }

    @Test
    public void testCountFamily() {
        Human mother = new Human("Jane", "Doe", 1980);
        Human father = new Human("John", "Doe", 1978);
        Family family = new Family(mother, father);
        Human child = new Human("Alice", "Doe",1999);
        family.addChild(child);

        assertEquals(3, family.countFamily());
    }
}
