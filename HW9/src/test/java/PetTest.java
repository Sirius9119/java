import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


public class PetTest {
    @Test
    void testToStringWithoutHabits() {
        Dog dog = new Dog("Sharik", 5, 85, new HashSet<>(Set.of("eat", "drink", "sleep")));
        String expected = "DOG{nickname='Sharik', age=5, trickLevel=85, habits=sleep, eat, drink}";

        assertEquals(expected, dog.toString());
    }

}
